//
//  ViewController.swift
//  CursoApp
//
//  Created by Oscar on 10/10/22.
//

import UIKit

class ViewController: UIViewController {
    enum Turno {
        case O
        case X
    }
    
    @IBOutlet weak var txtResultado: UILabel!
    @IBOutlet weak var boton1: UIButton!
    @IBOutlet weak var boton2: UIButton!
    @IBOutlet weak var boton3: UIButton!
    @IBOutlet weak var boton4: UIButton!
    @IBOutlet weak var boton5: UIButton!
    @IBOutlet weak var boton6: UIButton!
    @IBOutlet weak var boton7: UIButton!
    @IBOutlet weak var boton8: UIButton!
    @IBOutlet weak var boton9: UIButton!
    
    var primerTurno = Turno.X
    var actualTurno = Turno.X
    
    var X = "X"
    var O = "O"
    var tablero = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inicializarTablero()
        
        
    }
    
    func inicializarTablero()
    {
        tablero.append(boton1)
        tablero.append(boton2)
        tablero.append(boton3)
        tablero.append(boton4)
        tablero.append(boton5)
        tablero.append(boton6)
        tablero.append(boton7)
        tablero.append(boton8)
        tablero.append(boton9)
    }
    
    @IBAction func boardTapAction(_ sender: UIButton) {
        
        addTabla(sender)
        
        if comprobarGanador(X){
            resultadoAlerta(title: "Gano el Jugador X")
        }
        if comprobarGanador(O){
            resultadoAlerta(title: "Gano el Jugador O")
        }
        
        if(TablaLlena()){
            resultadoAlerta(title: "Empate")
        }
    }
    
    func resetTablero(){
        
        for button in tablero {
            button.setTitle(nil, for: .normal)
            button.isEnabled = true
        }
        if primerTurno == Turno.O{
            primerTurno = Turno.X
            txtResultado.text = X
        }
        else if primerTurno == Turno.X{
            primerTurno = Turno.O
            txtResultado.text = O
        }
        actualTurno = primerTurno
    }
    
    func comprobarGanador(_ s :String) -> Bool{
        //Combinacines Horizontales
        if Simbolo(boton1, s) && Simbolo(boton2,s) && Simbolo(boton3, s){
            return true
        }
        if Simbolo(boton4, s) && Simbolo(boton5,s) && Simbolo(boton6, s){
            return true
        }
        if Simbolo(boton7, s) && Simbolo(boton8,s) && Simbolo(boton9, s){
            return true
        }
        //Combinacines Verticales
        if Simbolo(boton1, s) && Simbolo(boton4,s) && Simbolo(boton7, s){
            return true
        }
        if Simbolo(boton2, s) && Simbolo(boton5,s) && Simbolo(boton8, s){
            return true
        }
        if Simbolo(boton3, s) && Simbolo(boton6,s) && Simbolo(boton9, s){
            return true
        }
        //Combinacines Diagonales
        if Simbolo(boton1, s) && Simbolo(boton5,s) && Simbolo(boton9, s){
            return true
        }
        if Simbolo(boton3, s) && Simbolo(boton5,s) && Simbolo(boton7, s){
            return true
        }
        return false
    }
    
    func Simbolo(_ button: UIButton, _ symbol:String) -> Bool{
        return button.title(for: .normal) == symbol
    }
    
    func resultadoAlerta(title: String){
        let alertcontroller = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        alertcontroller.addAction(UIAlertAction(title: "Volver a Jugar", style: .default,handler: { (_) in
            self.resetTablero()
        }))
        self.present(alertcontroller, animated: true)
    }
    
    func TablaLlena() -> Bool{
        for button in tablero {
            if button.title(for: .normal) == nil{
                return false
            }
        }
        return true
    }
    
    func addTabla(_ sender: UIButton){
        if(sender.title(for: .normal) == nil){
            if(actualTurno == Turno.O){
                sender.setTitle(O, for: .normal)
                actualTurno = Turno.X
                txtResultado.text = X
            }
            else if(actualTurno == Turno.X){
                sender.setTitle(X, for: .normal)
                actualTurno = Turno.O
                txtResultado.text = O
            }
            sender.isEnabled = false
        }

    }
}
